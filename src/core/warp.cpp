/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "warp.h"
#include "frame.h"
#include "vector.h"

Point2f Warp::squareToUniformSquare(const Point2f &sample) { 
  return sample; }

float Warp::squareToUniformSquarePdf(const Point2f &sample) {
  return ((sample.array() >= 0).all() && (sample.array() <= 1).all()) ? 1.0f
                                                                      : 0.0f;
}

Vector3f Warp::squareToUniformSphere(const Point2f &sample) {
  float z = 1.f - 2.f * sample.x();
  float r = std::sqrt(std::max(0.f, 1.f - z * z));
  float phi = 2.f * M_PI * sample.y();
  return Vector3f(r * std::cos(phi), r * std::sin(phi), z);
}

float Warp::squareToUniformSpherePdf(const Vector3f &v) { return INV_FOURPI; }

Point2f Warp::squareToUniformDisk(const Point2f &sample) {
  float r = std::sqrt(sample.x());
  float phi = 2 * M_PI * sample.y();
  //transformation coordonnées polaire en cartésienne
  float x = r * std::cos(phi);
  float y = r * std::sin(phi);
  return Point2f(x,y);
}

float Warp::squareToUniformDiskPdf(const Point2f &p) {
  //norme de  p c'est la longueur, pour savoir si c'est dans le cercle
  if(p.norm() <= 1) {
    //dans le cercle
    return INV_PI;
  }
  //hors du cercle
  return 0.f;
}

Vector3f Warp::squareToUniformHemisphere(const Point2f &sample) {
  float phi = 2.f * M_PI * sample.x();//l'angle horizontal
  float teta = std::acos(sample.y());//l'angle vertical
  
  Vector3f u = Vector3f(1,0,0);
  Vector3f v = Vector3f(0,1,0);
  Vector3f w = Vector3f(0,0,1);

  Vector3f p = std::sin(teta) * std::cos(phi) * u + 
              std::sin(teta) * std::sin(phi) * v + 
              sample.y() * w;

  return p;
}

float Warp::squareToUniformHemispherePdf(const Vector3f &v) {
  float d = pow(v.x(),2) + pow(v.y(),2) + pow(v.z(),2);

  if(sqrt(d) < 1-Epsilon || v.z() < 0||sqrt(d) > 1+Epsilon) {
    return 0.f;
    //hors de l'hémisphère
    
  }else {
  //dans l'hémisphère
  return 1/(2* M_PI);
  }
  
}

Vector3f Warp::squareToCosineHemisphere(const Point2f &sample) {
  float phi = 2.f * M_PI * sample.x();//l'angle horizontal
  float teta = std::acos(std::sqrt(1-sample.y()));//l'angle vertical
  
  Vector3f u = Vector3f(1,0,0);
  Vector3f v = Vector3f(0,1,0);
  Vector3f w = Vector3f(0,0,1);

  Vector3f p = std::sin(teta) * std::cos(phi) * u + 
              std::sin(teta) * std::sin(phi) * v + 
              std::cos(teta) * w;

  return p;
}

float Warp::squareToCosineHemispherePdf(const Vector3f &v) {
  if(v.z() > 0) {
    //dans l'hémisphère
    return 1/M_PI * v.z();
  }
  return 0.f;
}

Point2f Warp::squareToUniformTriangle(const Point2f &sample) {
  throw(RTException("Warp::squareToUniformTriangle not implemented yet"));
  return Point2f::Zero();
}

float Warp::squareToUniformTrianglePdf(const Point2f &p) {
  throw(RTException("Warp::squareToUniformTrianglePdf not implemented yet"));
  return 0.f;
}

Vector3f Warp::squareToBeckmann(const Point2f &sample, float alpha) {
  float phi = 2.f * M_PI *sample.x();
  float alpha2 = pow(alpha,2);

  float theta = atan( sqrt( -alpha2 * log(1 - sample.y()) ) );
  
  float x = sin(theta) * cos(phi);
  float y = sin(theta) * sin(phi);
  float z = cos(theta);

  return Vector3f(x,y,z);
}

float Warp::squareToBeckmannPdf(const Vector3f &m, float alpha) {
  float cosTheta = Frame::cosTheta(m);
  if( cosTheta <=0 ) {
    return 0.f;
  }
  float tanteta = Frame::tanTheta(m);
  float teta = acos(cosTheta);
  float sinteta = sin(teta);

  float tan2 = pow(tanteta,2);
  float cos3 = pow(cosTheta,3); 
  float alpha2 = pow(alpha,2);

  float p2 = 2* exp((-tan2)/alpha2);
  float p3 = alpha2 * cos3;

  return INV_TWOPI * (p2/p3);
}
