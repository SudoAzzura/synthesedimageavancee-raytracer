
/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob
*/

#include "bsdf.h"
#include "core/warp.h"
#include "frame.h"
#include <math.h>


class Microfacet : public BSDF {
public:
  Microfacet(const PropertyList &propList) {
    /* RMS surface roughness */
    m_alpha = propList.getFloat("alpha", 0.1f);

    /* Interior IOR (default: BK7 borosilicate optical glass) */
    m_intIOR = propList.getFloat("intIOR", 1.5046f);

    /* Exterior IOR (default: air) */
    m_extIOR = propList.getFloat("extIOR", 1.000277f);

    /* Albedo of the diffuse base material (a.k.a "kd") */
    m_kd = propList.getColor("kd", Color3f(0.5f));

    /* To ensure energy conservation, we must scale the
       specular component by 1-kd.

       While that is not a particularly realistic model of what
       happens in reality, this will greatly simplify the
       implementation. */
    m_ks = 1 - m_kd.maxCoeff();
  }

  /// Evaluate the BRDF for the given pair of directions
  Color3f eval(const BSDFQueryRecord &bRec) const {
    Vector3f WI = bRec.wi;
    Vector3f WO = bRec.wo;

    //float normeEuc = sqrt((WI + WO).dot(WI + WO));
    float normeEuc = sqrt(pow(WI.x()+WO.x(),2) + pow(WI.y()+WO.y(),2) + pow(WI.z()+WO.z(),2));
    Vector3f wh = (WI + WO) / normeEuc ;

    Vector3f n = Vector3f(WI.x()+WO.x(),WI.y()+WO.y(),WI.z()+WO.z());

    float cosThetaI = WI.dot(n);
    float cosThetaO = WO.dot(n);
    //float cosThetaO = CosTetha(WI,WO,wh,n);
    float cosThetaH = wh.dot(n);
    float cosThetaT = 0;
    float cosThetaD = wh.dot(WI);
    float F = fresnel(cosThetaD,m_extIOR,m_intIOR,cosThetaT);

    float DFG = D(wh) *F * G(WI,WO,wh,n);
    Color3f fr = (m_kd / M_PI) +  m_ks *(DFG/(4*cosThetaI*cosThetaO*cosThetaH));
    
    return fr;
  }
  
  float D(Vector3f wh) const {
    return Warp::squareToBeckmannPdf(wh,m_alpha);
  }
  float CosTetha(Vector3f wi,Vector3f wo,Vector3f wh, Vector3f n) const{
    return (G(wi,wo,wh,n) * Warp::squareToBeckmannPdf(wh,m_alpha) * wo.dot(wh));
  }

  float G(Vector3f wi,Vector3f wo, Vector3f wh, Vector3f n) const{
    return G1(wi,wh,n) * G1(wo,wh,n);
  }

  float G1 (Vector3f wv, Vector3f wh, Vector3f n) const{
    float xplus = xPlus(wv,wh);
    float midForm = (wv.dot(wh))/(wv.dot(n));
    float angle = n.dot(wv)/ (n.normalized()).dot(wv.normalized());
    float Rb = R(b(radToDeg(angle)));
    return xplus * midForm * Rb;
  }

  float xPlus(Vector3f wo,Vector3f wh) const{
    float c = wo.dot(wh);
    if(c>0) {
      return 1.f;
    } else {
      return 0.f;
    }
  }

  float b(float angle) const {//angle entre la normal de la surface n et et w_v de G
    //cout<<pow(m_alpha *tan(angle),-1)<<endl;
    return radToDeg(pow(m_alpha *tan(angle),-1));
  }

  float R(float b) const{
    if(b<1.6) {
      return (3.535* b + 2.181*b*b)/(1 + 2.276*b + 2.577*b*b);
    }else {
      return 1.f;
    }
  }

  

  /// Evaluate the sampling density of \ref sample() wrt. solid angles
  float pdf(const BSDFQueryRecord &bRec) const {
    throw RTException("MicrofacetBRDF::pdf not implemented yet");
    return 0.f;
  }

  /// Sample the BRDF
  Color3f sample(BSDFQueryRecord &bRec, const Point2f &sample) const {
    //bRec.wo = Warp::squareToBeckmann(sample,m_alpha);
    return Color3f(0.f);
  }

  bool isDiffuse() const {
    /* While microfacet BRDFs are not perfectly diffuse, they can be
       handled by sampling techniques for diffuse/non-specular materials,
       hence we return true here */
    return true;
  }

  std::string toString() const {
    return tfm::format("Microfacet[\n"
                       "  alpha = %f,\n"
                       "  intIOR = %f,\n"
                       "  extIOR = %f,\n"
                       "  kd = %s,\n"
                       "  ks = %f\n"
                       "]",
                       m_alpha, m_intIOR, m_extIOR, m_kd.toString(), m_ks);
  }

private:
  float m_alpha;
  float m_intIOR, m_extIOR;
  float m_ks;
  Color3f m_kd;
};

REGISTER_CLASS(Microfacet, "microfacet");
