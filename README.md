# TD4 - SIA - raytracer - Chloé Neuville
## 1. Ambiant Occlusion
### 1.1. Échauffement : échantillonnage du disque unitaire
![alt text](./dataMd/Disk.png)
![alt text](./dataMd/Echantillonner_un_disque.png)
Si pour les deux il y avait eu une erreur ( par exemple, un point hors du cercle), le test aussi aurait été bon mais d'après mon jugement visuelle je pourrai affirmer que le test est faux.
Dans le cas de notre test, il est bon, et visuellement, il l'est aussi, tous les points sont bien dans le disque.
### 1.2 Échantillonnage de l'hémisphère unitaire
#### Warp::squareToUniformHemisphere 
Au début, j'avais ce résultat :  
  
  ![alt text](./dataMd/bugh.png)
  ![alt text](./dataMd/bugh2.png)
    
  qui donnait ça en test :    

  ![alt text](./dataMd/bugh3.png)

  C'était certain que mon problème venait de la formule créant les points dans l'échantillonnage,une erreur d'inattention où j'ai mis phi au lieu de teta dans une partie du calcul.

Et j'ai enfin eu le résultat tant attendu: 
  
![alt text](./dataMd/HemisphereOk.png)
![alt text](./dataMd/HemisphereOk2.png)
![alt text](./dataMd/HemisphereTestOk.png)


#### Warp::squareToCosineHemisphere et Warp::squareToCosineHemispherePdf
![alt text](./dataMd/HcosOk.png)
###  1.3 Calcul de l'AO
#### killeroo_ao.scn, sample = 32, cosine-weighted = false, time = 1.4s
![alt text](./dataMd/DinoCosfalse32.png)
#### killeroo_ao.scn, sample = 32, cosine-weighted = true, time = 1.1s
![alt text](./dataMd/DinoCostrue32.png)

La fonction pondéré par le cosinus est bien mieux, d'une part elle donne une image moins bruitée et d'autre part elle est plus rapide.